--Création des tables--

create table Nutrient (id serial primary key AUTO_INCREMENT,name text not null);

create table Food (id text, name text not null, grp_id text not null, ssgrp_id text not null, ssssgrp_id text null);

create table Grp (id text, name text not null);

create table SSGrp (id text, name text not null, grp_id text);

create table SSSSGrp (id text, name text not null, name text not null, ssgrp_id text not null);

create table NutData (id integer primary key AUTO_INCREMENT, food_id text not null, nutrient_id integer, value text not null);


--Insertion--

INSERT INTO Nutrient (name) VALUES ("Energie, Règlement UE N° 1169/2011 (kJ/100 g)");
INSERT INTO Nutrient (name) VALUES ("Energie, Règlement UE N° 1169/2011 (kcal/100 g)");
INSERT INTO Nutrient (name) VALUES ("Energie, N x facteur Jones, avec fibres  (kJ/100 g)");
INSERT INTO Nutrient (name) VALUES ("Energie, N x facteur Jones, avec fibres  (kcal/100 g)");
INSERT INTO Nutrient (name) VALUES ("Eau (g/100 g)");
INSERT INTO Nutrient (name) VALUES ("Protéines, N x facteur de Jones (g/100 g)");
INSERT INTO Nutrient (name) VALUES ("Protéines, N x 6.25 (g/100 g)");
INSERT INTO Nutrient (name) VALUES ("Glucides (g/100 g)");

INSERT INTO Food (id, name) VALUES (24999,"Dessert (aliment moyen)");
INSERT INTO Food (id, name) VALUES (25601,"Salade de thon et légumes, appertisée");
INSERT INTO Food (id, name) VALUES (25602,"Salade composée avec viande ou poisson, appertisée");
INSERT INTO Food (id, name) VALUES (25605,"Champignons à la grecque, appertisés");
INSERT INTO Food (id, name) VALUES (25606,"Salade de pommes de terre, fait maison");
INSERT INTO Food (id, name) VALUES (25608,"Taboulé ou Salade de couscous, préemballé");
INSERT INTO Food (id, name) VALUES (25609,"Salade de pomme de terre à la piémontaise, préemballée");
INSERT INTO Food (id, name) VALUES (25614,"Salade de riz, appertisée");
INSERT INTO Food (id, name) VALUES (25615,"Salade de pâtes, végétarienne");
INSERT INTO Food (id, name) VALUES (25616,"Crudité, sans assaisonnement (aliment moyen)");
INSERT INTO Food (id, name) VALUES (25619,"Salade de pâtes aux légumes, avec poisson ou viande, préemballée");

INSERT INTO Grp (id, name) VALUES (00,"");
INSERT INTO Grp (id, name) VALUES (01,"entrées et plats composés");
INSERT INTO Grp (id, name) VALUES (02,"fruits, légumes, légumineuses et oléagineux");
INSERT INTO Grp (id, name) VALUES (03,"produits céréaliers");
INSERT INTO Grp (id, name) VALUES (03,"");
INSERT INTO Grp (id, name) VALUES (04,"viandes, œufs, poissons et assimilés");
INSERT INTO Grp (id, name) VALUES (05,"produits laitiers et assimilés");
INSERT INTO Grp (id, name) VALUES (06,"eaux et autres boissons");
INSERT INTO Grp (id, name) VALUES (07,"produits sucrés");
INSERT INTO Grp (id, name) VALUES (08,"glaces et sorbets");
INSERT INTO Grp (id, name) VALUES (09,"matières grasses");

INSERT INTO SSGrp (grp_id, id, name) VALUES (0,000,"");
INSERT INTO SSGrp (grp_id, id, name) VALUES (0,101,"salades composées et crudités");
INSERT INTO SSGrp (grp_id, id, name) VALUES (0,102,"soupes");
INSERT INTO SSGrp (grp_id, id, name) VALUES (0,103,"plats composés");
INSERT INTO SSGrp (grp_id, id, name) VALUES (0,104,"pizzas, tartes et crêpes salées");
INSERT INTO SSGrp (grp_id, id, name) VALUES (0,105,"sandwichs");
INSERT INTO SSGrp (grp_id, id, name) VALUES (0,106,"feuilletées et autres entrées");
INSERT INTO SSGrp (grp_id, id, name) VALUES (0,201,"légumes");
INSERT INTO SSGrp (grp_id, id, name) VALUES (0,202,"pommes de terre et autres tubercules");
INSERT INTO SSGrp (grp_id, id, name) VALUES (0,203,"légumineuses");

INSERT INTO SSSSGrp (id, ssgrp_id, name) VALUES (0000,000000,"");
INSERT INTO SSSSGrp (id, ssgrp_id, name) VALUES (0101,000000,"-");
INSERT INTO SSSSGrp (id, ssgrp_id, name) VALUES (0102,000000,"-");
INSERT INTO SSSSGrp (id, ssgrp_id, name) VALUES (0103,010301,"plats de viande sans garniture");
INSERT INTO SSSSGrp (id, ssgrp_id, name) VALUES (0103,010302,"plats de viande et féculents");
INSERT INTO SSSSGrp (id, ssgrp_id, name) VALUES (0103,010303,"plats de viande et légumes/légumineuses");
INSERT INTO SSSSGrp (id, ssgrp_id, name) VALUES (0103,010304,"plats de poisson sans garniture");
INSERT INTO SSSSGrp (id, ssgrp_id, name) VALUES (0103,010305,"plats de poisson et féculents");
INSERT INTO SSSSGrp (id, ssgrp_id, name) VALUES (0103,010306,"plats de légumes/légumineuses");
INSERT INTO SSSSGrp (id, ssgrp_id, name) VALUES (0103,010307,"plats de céréales/pâtes");

INSERT INTO NutData (food_id, value, nutrient_id) VALUES (24999,"",0);
INSERT INTO NutData (food_id, value, nutrient_id) VALUES (25601,"-",0);
INSERT INTO NutData (food_id, value, nutrient_id) VALUES (25602,"-",0);
INSERT INTO NutData (food_id, value, nutrient_id) VALUES (25605,"-",0);
INSERT INTO NutData (food_id, value, nutrient_id) VALUES (25606,"-",0);
INSERT INTO NutData (food_id, value, nutrient_id) VALUES (25608,"753",0);
INSERT INTO NutData (food_id, value, nutrient_id) VALUES (25609,"542",0);
INSERT INTO NutData (food_id, value, nutrient_id) VALUES (25614,"-",0);
INSERT INTO NutData (food_id, value, nutrient_id) VALUES (25615,"-",0);
INSERT INTO NutData (food_id, value, nutrient_id) VALUES (25616,"125",0);





